import { Entity } from "../library/classes/Entity";
import { Vector2 } from "../library/utilities/Vector2";
import { Renderer } from "../library/modules/Renderer";
import { Collsions } from "../library/modules/Collisions";

const CELL_SIZE = new Vector2(16, 16);
const PLAYER_SIZE = new Vector2(CELL_SIZE.x - 2, CELL_SIZE.y * 1.25);
const PLAYER_HEIGHT_CROUCHING = CELL_SIZE.y - 2;

const level = 1;
const entities = new Set<Entity>();

const gravity   = 15;
const friction  = -9;

const player = new Entity(
    new Vector2(2 * CELL_SIZE.x, 4 * CELL_SIZE.y),
    new Vector2(PLAYER_SIZE.x, PLAYER_SIZE.y),
    "#50a9cf"
);

let isOnGround = false;

const Actions = {
    JUMP: false,
    CROUCH: false,
    STAND_UP: false,
    MOVE_LEFT: false,
    MOVE_RIGHT: false
};

const TileTypes = {
    SOLID: 11
} as const;

async function setup() {

    entities.add(player);

    const {
        cols,
        layout
    }: MapData = await import(`./maps/level-${level}.map.json`);

    for (let i = 0; i < layout.length; i++) {
        if (layout[i] === TileTypes.SOLID) {
            entities.add(new Entity(
                new Vector2(
                    i % cols * CELL_SIZE.x,
                    Math.floor(i / cols) * CELL_SIZE.y
                ),
                CELL_SIZE,
                "#bd4b64"
            ));
        }
    }

    window.addEventListener("keydown", (event) => {
        if (event.key === " ") Actions.JUMP = isOnGround;
        if (event.key === "s") Actions.CROUCH = true;
        if (event.key === "a") Actions.MOVE_LEFT = true;
        if (event.key === "d") Actions.MOVE_RIGHT = true;
    });

    window.addEventListener("keyup", (event) => {
        if (event.key === "s") Actions.STAND_UP = true;
        if (event.key === "a") Actions.MOVE_LEFT = false;
        if (event.key === "d") Actions.MOVE_RIGHT = false;
    });

    window.addEventListener("blur", () => {
        for (let action in Actions) {
            Actions[action as keyof typeof Actions] = false;
        }
    });
}

function update(deltaTime: number) {

    if (player.box.right < 0) {
        player.pos.x = Renderer.canvas.width;
        player.pos.y -= 1;
    }

    if (player.box.left > Renderer.canvas.width) {
        player.pos.x = -player.size.x;
        player.pos.y -= 1;
    }

    if (player.box.top > Renderer.canvas.height) {
        player.pos.y = -player.size.y;
    }

    if (Actions.JUMP) {
        isOnGround = false;
        Actions.JUMP = false;
        player.velocity.y = -gravity / 2.75;
    }

    if (Actions.CROUCH && player.size.y === PLAYER_SIZE.y) {
        player.pos.y += player.size.y - PLAYER_HEIGHT_CROUCHING;
        player.size.y = PLAYER_HEIGHT_CROUCHING;

        Actions.STAND_UP = false;
    }

    if (Actions.STAND_UP && player.size.y !== PLAYER_SIZE.y) {

        player.pos.y -= PLAYER_SIZE.y - PLAYER_HEIGHT_CROUCHING;
        player.size.y = PLAYER_SIZE.y;

        let canStandUp = true;

        entities.forEach(entity => {
            if (entity.id === player.id || !canStandUp) return;
            if (Collsions.checkAabbVsAabb(player, entity)) canStandUp = false;
        });

        if (canStandUp) {
            Actions.CROUCH = false;
        } else {
            player.pos.y += player.size.y - PLAYER_HEIGHT_CROUCHING;
            player.size.y = PLAYER_HEIGHT_CROUCHING;
        }
    }

    if (Actions.MOVE_LEFT) {
        player.velocity.x -= 20 * deltaTime;
    }

    if (Actions.MOVE_RIGHT) {
        player.velocity.x += 20 * deltaTime;
    }

    player.velocity.y += gravity * deltaTime;
    player.velocity.x *= Math.pow(Math.E, friction * deltaTime);

    entities.forEach(entity => {

        Renderer.drawRectangle(entity.pos, entity.size, entity.color);

        // // RENDER DEBUG INFO
        // if (entity.id === player.id) {
        //     Renderer.drawLine(
        //         player.box.center,
        //         player.box.center.clone().add(player.velocity.clone().normalize().mulScalar(5)),
        //         "#FFFFFF"
        //     );
        // } else {
        //     Renderer.drawText(entity.pos.clone().add(new Vector2(4, 8)), entity.id, "7px sans-serif");
        // }
    });

    checkAndResolvePlayerCollisions();
    player.pos.add(player.velocity);
}

function resolveCornerCollisions() {

    const temp = player.velocity.clone();

    let canMoveOnXAxis = true;
    player.velocity.set(temp.x, 0);

    entities.forEach(entity => {
        if (entity.id === player.id || !canMoveOnXAxis) return;
        if (Collsions.checkAabbVsAabb(player, entity)) canMoveOnXAxis = false;
    });

    if (canMoveOnXAxis) return;

    let canMoveOnYAxis = true;
    player.velocity.set(0, temp.y);

    entities.forEach(entity => {
        if (entity.id === player.id || !canMoveOnYAxis) return;
        if (Collsions.checkAabbVsAabb(player, entity)) canMoveOnYAxis = false;
    });

    if (canMoveOnYAxis) return;

    player.velocity.set(0, 0);
}

function checkAndResolvePlayerCollisions() {

    const collisions: NonNullable<Collision>[] = [];

    entities.forEach(entity => {
        if (entity.id === player.id) return;
        const collision = Collsions.checkSweptAabbVsAabb(player, entity);
        if (collision) collisions.push(collision);
    });

    collisions.sort((a, b) => {
        if (a.time < b.time) return -1;
        if (a.time > b.time) return 1;
        return 0;
    });

    isOnGround = false;

    collisions.forEach(({ other }) => {

        const collision = Collsions.checkSweptAabbVsAabb(player, other);

        if (collision) {

            if (collision.normal.x === 0 && collision.normal.y === 0) {
                resolveCornerCollisions();
            }

            if (!isOnGround) {
                isOnGround = collision.normal.y < 0;
            }

            player.velocity.add(
                collision.normal
                    .clone()
                    .mul(player.velocity.clone().abs())
                    .mulScalar(1 - collision.time)
            );
        }
    });
}

export default {
    setup,
    update
};
