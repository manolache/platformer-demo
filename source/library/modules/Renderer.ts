import { Vector2 } from "../utilities/Vector2";

export abstract class Renderer {

    static canvas = document.createElement("canvas");
    static context = <CanvasRenderingContext2D>Renderer.canvas.getContext("2d");
    static resolution = new Vector2(320, 240);

    static setup() {

        this.canvas.setAttribute("tabindex", "0");

        Renderer.canvas.width = Renderer.resolution.x;
        Renderer.canvas.height = Renderer.resolution.y;

        Renderer.canvas.style.setProperty(
            "--ratio",
            `${Renderer.resolution.x / Renderer.resolution.y}`
        );

        Renderer.canvas.style.imageRendering = "pixelated";

        document.body.prepend(Renderer.canvas);
    }

    static clear() {
        Renderer.context.fillStyle = "#111111";
        Renderer.context.fillRect(0, 0, Renderer.canvas.width, Renderer.canvas.height);
    }

    static drawText(
        pos: Vector2,
        text: number | string,
        font: string = "10px sans-serif",
        color: string = "#FFFFFF"
    ) {
        Renderer.context.font = font;
        Renderer.context.fillStyle = color;
        Renderer.context.fillText(String(text), pos.x, pos.y);
    }

    static drawLine(start: Vector2, end: Vector2, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 1;
        Renderer.context.strokeStyle = color;

        Renderer.context.beginPath();
        Renderer.context.moveTo(Math.round(start.x), Math.round(start.y));
        Renderer.context.lineTo(Math.round(end.x), Math.round(end.y));
        Renderer.context.closePath();
        Renderer.context.stroke();
    }

    static drawCircle(position: Vector2, radius: number, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 2;
        Renderer.context.fillStyle = color;

        Renderer.context.beginPath();
        Renderer.context.arc(position.x, position.y, radius, 0, 2 * Math.PI);
        Renderer.context.closePath();
        Renderer.context.fill();
    }

    static drawCircleOutline(position: Vector2, radius: number, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 2;
        Renderer.context.strokeStyle = color;

        Renderer.context.beginPath();
        Renderer.context.arc(position.x, position.y, radius, 0, 2 * Math.PI);
        Renderer.context.closePath();
        Renderer.context.stroke();
    }

    static drawRectangle(position: Vector2, size: Vector2, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 1;
        Renderer.context.fillStyle = color;

        Renderer.context.fillRect(
            Math.round(position.x),
            Math.round(position.y),
            size.x,
            size.y
        );
    }

    static drawRectangleOutline(position: Vector2, size: Vector2, color: string = "#FF00FF") {

        Renderer.context.lineWidth = 1;
        Renderer.context.strokeStyle = color;

        Renderer.context.strokeRect(
            Math.round(position.x),
            Math.round(position.y),
            size.x,
            size.y
        );
    }
}
