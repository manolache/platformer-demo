import { Entity } from "../classes/Entity";
import { Vector2 } from "../utilities/Vector2";

let tempValue;
const tempEntity = new Entity();

function checkRayVsAabb(raySrc: Vector2, rayDir: Vector2, entity: Entity): Collision {

    const tNear = entity.pos.clone().sub(raySrc).div(rayDir);
    const tFar  = entity.pos.clone().add(entity.size).sub(raySrc).div(rayDir);

    if (tNear.x > tFar.x) {
        tempValue   = tNear.x;
        tNear.x     = tFar.x;
        tFar.x      = tempValue;
    }

    if (tNear.y > tFar.y) {
        tempValue   = tNear.y;
        tNear.y     = tFar.y;
        tFar.y      = tempValue;
    }

    if (tNear.x > tFar.y || tNear.y > tFar.x) return;

    const tHitNear  = Math.max(tNear.x, tNear.y);
    const tHitFar   = Math.min(tFar.x, tFar.y);

    if (tHitNear < 0 || tHitNear >= 1 || tHitFar < 0) return;

    const contactPoint  = raySrc.clone().add(rayDir.clone().mulScalar(tHitNear));
    const contactNormal = new Vector2();

    if (tNear.x > tNear.y) {
        rayDir.x < 0
            ? contactNormal.set(1, 0)
            : contactNormal.set(-1, 0);
    } else if (tNear.x < tNear.y) {
        rayDir.y < 0
            ? contactNormal.set(0, 1)
            : contactNormal.set(0, -1);
    }

    return {
        time: tHitNear,
        other: entity,
        normal: contactNormal,
        contact: contactPoint
    };
}

function checkAabbVsAabb(entityA: Entity, entityB: Entity): boolean {
    return (
        entityA.box.right + entityA.velocity.x > entityB.box.left &&
        entityA.box.left + entityA.velocity.x < entityB.box.right &&
        entityA.box.bottom + entityA.velocity.y > entityB.box.top &&
        entityA.box.top + entityA.velocity.y < entityB.box.bottom
    );
}

function checkSweptAabbVsAabb(entityA: Entity, entityB: Entity): Collision {

    if (!checkAabbVsAabb(entityA, entityB)) return;

    tempEntity.pos.copy(entityB.pos.clone().sub(entityA.size.clone().divScalar(2)));
    tempEntity.size.copy(entityB.size.clone().add(entityA.size));

    const collision = checkRayVsAabb(
        entityA.box.center,
        entityA.velocity,
        tempEntity
    );

    if (collision) {
        collision.other = entityB;
    }

    return collision;
}

export class Collsions {
    static checkRayVsAabb = checkRayVsAabb;
    static checkAabbVsAabb = checkAabbVsAabb;
    static checkSweptAabbVsAabb = checkSweptAabbVsAabb;
};
