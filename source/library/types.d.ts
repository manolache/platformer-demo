type Entity = import("./classes/Entity").Entity;
type Vector2 = import("./utilities/Vector2").Vector2;
type BoundingBox = import("./classes/BoundingBox").BoundingBox;

type MapData = {
    name: string;
    cols: number;
    rows: number;
    layout: number[];
};

type Collision = {
    time: number;
    other: Entity;
    normal: Vector2;
    contact: Vector2;
} | undefined;
