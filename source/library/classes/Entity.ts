import { Vector2 } from "../utilities/Vector2";
import { BoundingBox } from "./BoundingBox";
import { IdGenerator } from "../utilities/IdGenerator";

export class Entity {

    constructor(
        readonly pos: Vector2 = new Vector2(),
        readonly size: Vector2 = new Vector2(16, 16),
        public color: string = "#FF00FF"
    ) {
        // ...
    }

    readonly id = IdGenerator.generate();
    readonly box = new BoundingBox(this);
    readonly velocity = new Vector2();
}
