export class BoundingBox {

    constructor(
        private entity: Entity
    ) {
        // ...
    }

    get pos() {
        return this.entity.pos;
    }

    get size() {
        return this.entity.size;
    }

    get top() {
        return this.entity.pos.y;
    }

    get left() {
        return this.entity.pos.x;
    }

    get right() {
        return this.entity.pos.x + this.entity.size.x;
    }

    get bottom() {
        return this.entity.pos.y + this.entity.size.y;
    }

    get center() {
        return this.entity.pos.clone().add(this.entity.size.clone().divScalar(2));
    }
}
