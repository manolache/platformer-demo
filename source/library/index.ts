import { Renderer } from "./modules/Renderer";

import script from "../game/main";

let rafHandle: number;
let isGamePaused = true;

(async function () {
    try {

        Renderer.setup();

        await script.setup();

        let deltaTimeMs = 0;
        let deltaTimeSec = 0;
        let prevFrameTimeMs = 0;
        let currFrameTimeMs = 0;

        isGamePaused = false;

        function loop() {

            if (isGamePaused) return;

            Renderer.clear();

            currFrameTimeMs = performance.now();

            deltaTimeMs = currFrameTimeMs - prevFrameTimeMs;
            deltaTimeSec = deltaTimeMs / 1000;

            prevFrameTimeMs = currFrameTimeMs;

            script.update(deltaTimeSec);

            rafHandle = requestAnimationFrame(loop);
        }

        window.addEventListener("blur", () => {
            isGamePaused = true;
            cancelAnimationFrame(rafHandle);
        });

        window.addEventListener("focus", () => {

            if (!isGamePaused) return;

            isGamePaused = false;
            prevFrameTimeMs = performance.now();

            rafHandle = requestAnimationFrame(loop);
        });

        rafHandle = requestAnimationFrame(loop);

    } catch(error) {
        console.error(error);
    }
})();
