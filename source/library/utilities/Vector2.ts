export class Vector2 {

    constructor(
        public x: number = 0,
        public y: number = 0
    ) {
        // ...
    }

    set(x: number, y: number): Vector2 {
        this.x = x;
        this.y = y;
        return this;
    }

    copy(other: Vector2): Vector2 {
        this.x = other.x;
        this.y = other.y;
        return this;
    }

    reset() {
        this.x = 0;
        this.y = 0;
    }

    clone(): Vector2 {
        return new Vector2(this.x, this.y);
    }

    abs(): Vector2 {
        this.x = Math.abs(this.x);
        this.y = Math.abs(this.y);
        return this;
    }

    add(other: Vector2): Vector2 {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    addScalar(value: number): Vector2 {
        this.x += value;
        this.y += value;
        return this;
    }

    sub(other: Vector2): Vector2 {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    subScalar(value: number): Vector2 {
        this.x -= value;
        this.y -= value;
        return this;
    }

    mul(other: Vector2): Vector2 {
        this.x *= other.x;
        this.y *= other.y;
        return this;
    }

    mulScalar(value: number): Vector2 {
        this.x *= value;
        this.y *= value;
        return this;
    }

    div(other: Vector2): Vector2 {
        this.x /= other.x;
        this.y /= other.y;
        return this;
    }

    divScalar(value: number): Vector2 {
        this.x /= value;
        this.y /= value;
        return this;
    }

    length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    normalize(): Vector2 {
        const length = this.length();
        this.x /= length;
        this.y /= length;
        return this;
    }
}
