let counter = 0;

export abstract class IdGenerator {
    static generate() {
        return counter++;
    }
}
